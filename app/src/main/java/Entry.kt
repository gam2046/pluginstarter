import android.app.Activity
import android.app.Application
import android.util.Log
import com.example.magisk_mount_bridge.BeanContainer
import com.example.magisk_mount_bridge.ModuleEntry
import com.example.magisk_mount_bridge.utils.LocalServiceLite
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class Entry : ModuleEntry{
    private val mEventBus = BeanContainer.get(EventBus::class.java)!!

    private val TAG = ""

    private fun failure(msg: String) {
        Log.d(TAG, "failure: $msg")
        LocalServiceLite.markInjuryFinish(false, msg, null)
    }

    private fun success() {
        Log.d(TAG, "success: ")
        LocalServiceLite.markInjuryFinish(false, "", null)
    }

    override fun onActivityHidden(
        classloader: ClassLoader,
        packageName: String,
        parameters: Map<String, String>?,
        activity: WeakReference<Activity>
    ) {

    }

    override fun onActivityShown(
        classloader: ClassLoader,
        packageName: String,
        parameters: Map<String, String>?,
        activity: WeakReference<Activity>
    ) {
    }

    override fun onApplicationCreate(
        classloader: ClassLoader,
        packageName: String,
        parameters: Map<String, String>?,
        application: Application
    ) {
    }

    override fun onLoad(
        classloader: ClassLoader,
        packageName: String,
        parameters: Map<String, String>?
    ) {
        // 当前应用的版本 可判断是否符合预期
        val versionCode = parameters?.get("versionCode")?.toLong()
        val versionName = parameters?.get("versionName")
    }
}
