package com.github.gam2046.bean

import android.os.Parcel
import android.os.Parcelable

//data class BinderResult(
//    var success: Boolean,
//    var message: String
//)

class BinderResult() : Parcelable {
    var success: Boolean = false
    var message: String = ""
    var extra: MutableMap<String, String?> = mutableMapOf()

    companion object CREATOR : Parcelable.Creator<BinderResult> {
        override fun createFromParcel(parcel: Parcel): BinderResult {
            return BinderResult(parcel)
        }

        override fun newArray(size: Int): Array<BinderResult> {
            return Array(size) { BinderResult() }
        }
    }

    private constructor(parcel: Parcel) : this() {
        this.readFromParcel(parcel)
    }

    constructor(success: Boolean, message: String) : this() {
        this.success = success
        this.message = message
    }

    private fun readFromParcel(parcel: Parcel) {
        if (parcel.readInt() != 0) {
            this.success = true
        }
        this.message = parcel.readString() ?: ""
        parcel.readMap(this.extra, null)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        if (this.success) {
            dest.writeInt(1)
        } else {
            dest.writeInt(0)
        }
        dest.writeString(message)
        dest.writeMap(this.extra)
    }
}