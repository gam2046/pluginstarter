                                                                                                                                                                                                                                                                                                              // ILocalTransferService.aidl
package com.github.gam2046.services;

import com.github.gam2046.bean.BinderResult;

interface ILocalTransferService {
    /* 当前服务端版本 */
    int version();
    /* 以root身份执行命令行 */
    BinderResult shell(in String[] cmd);
    /* 标记任务状态 */
    void finishTask(boolean success, in Map parameters);
    /* 任务中沟通 */
    BinderResult communicateTask(in Map parameters);
    /* 检查应用是否注入 */
    BinderResult checkInjury(in String packageName);
    /* 全局单击 */
    void touch(int x, int y);
    /* 全局滑动 */
    void swipe(int sx, int sy, int ex, int ey);
}
